const ftoc = function(tempF) {
    cTemp = (tempF - 32) * (5/9);
    if (Number.isInteger(cTemp))
        return Math.round(cTemp);
    else 
        return Math.round(cTemp*10)/10;
};

const ctof = function(tempC) {
    fTemp = tempC * (9/5) + 32;
    if (Number.isInteger(fTemp))
        return Math.round(fTemp);
    else 
        return Math.round(fTemp*10)/10;
};

// Do not edit below this line
module.exports = {
  ftoc,
  ctof
};
