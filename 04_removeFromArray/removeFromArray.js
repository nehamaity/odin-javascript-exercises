const removeFromArray = function(array, ...numbers) {
    for (let i = 0; i < numbers.length; i++) {
         for (let j = 0; j < array.length; j++) {
            if (array[j] === numbers[i]) {
                array.splice(j, 1);
            }
        }
    }
    return array;
};

// Do not edit below this line
module.exports = removeFromArray;
